
## **TODO:**
- add a translation, after adding a new word.
- convert project from classes to hooks.
- create a button for editing words
- add pagination
- add few pages with different types of practice.
- fix react-router.
- ...



This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
