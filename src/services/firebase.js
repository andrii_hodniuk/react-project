import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';


const firebaseConfig = {
	apiKey: 'AIzaSyAsiZFXMsMZ90FOad2HtnlmaMIU6a1QIg8',
	authDomain: process.env.REACT_APP_AUTH_DOMAIN,
	databaseURL: 'https://learn-woords-online.firebaseio.com',
	projectId: process.env.REACT_APP_PROJECT_ID,
	storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
	messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
	appId: process.env.REACT_APP_APIAPP_ID_KEY,
};

class Firebase {
	constructor() {
		firebase.initializeApp(firebaseConfig);
		this.auth = firebase.auth();
		this.database = firebase.database();
	}
	signWithEmail = (email, password) => this.auth.signInWithEmailAndPassword(email, password);
	createUser = (email, password) => this.auth.createUserWithEmailAndPassword(email, password);
	logOut = () => this.auth.signOut();
}

const fb = new Firebase();

export default fb;