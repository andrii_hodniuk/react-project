import {CLEAR_USER_INFO, SET_USER_INFO} from '../types';

const initialState = {
	currentUser: null,
	userUid: '',
	userEmail: '',
};

export function userReducer(state = initialState, action) {
	switch (action.type) {
		case SET_USER_INFO:
			return {
				...state,
				currentUser: action.payload,
				userUid: action.payload.uid,
				userEmail: action.payload.email,
			};
		case CLEAR_USER_INFO:
			return {
				...state,
				currentUser: action.payload,
				userUid: action.payload,
				userEmail: action.payload,
			};
		default:
			return state;
	}
}