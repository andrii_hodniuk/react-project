import {
	ADD_NEW_WORD,
	CLEAR_INPUT_WORDS, CLEAR_SEARCHED_DICTIONARY,
	DELETE_WORD,
	GET_USER_DICTIONARY,
	INPUT_WORDS,
	SET_LEARNED_WORD, SET_SEARCHED_DICTIONARY,
} from '../types';

const initialState = {
	currentUserDictionary: [],
	searchedDictionary: null,
	inputWords: [],
	newWord: [],
	refresh: false,
};

export function dataReducer(state = initialState, action) {
	switch (action.type) {
		case GET_USER_DICTIONARY:
			return {...state, currentUserDictionary: action.payload};
		case SET_LEARNED_WORD:
			return {...state, currentUserDictionary: action.payload};
		case ADD_NEW_WORD:
			return {...action.payload.newWord, ...state, inputWords: action.payload.inputWords};
		case DELETE_WORD:
			return {...state, currentUserDictionary: action.payload};
		case INPUT_WORDS:
			return {...state, inputWords: action.payload};
		case CLEAR_INPUT_WORDS:
			return {...state, inputWords: action.payload};
		case SET_SEARCHED_DICTIONARY:
			return {...state, ...action.payload};
		case CLEAR_SEARCHED_DICTIONARY:
			return {...state, ...action.payload};
		default:
			return state;
	}
}