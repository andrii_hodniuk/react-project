import {combineReducers} from 'redux';
import {userReducer} from './userReducer';
import {uiReducer} from './uiReducer';
import {dataReducer} from './dataReducer';

export default combineReducers({
	user: userReducer,
	ui: uiReducer,
	data: dataReducer,
});