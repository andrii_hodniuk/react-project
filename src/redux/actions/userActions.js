import {CLEAR_USER_INFO, SET_USER_INFO} from '../types';
import fb from '../../services/firebase'

export function setUserInfo() {
	return dispatch => (
		fb.auth.onAuthStateChanged((user) => {
			if (user) {
				dispatch({type: SET_USER_INFO, payload: user});
			} else {
				dispatch({type: CLEAR_USER_INFO, payload: false});
			}
		})
	);
}