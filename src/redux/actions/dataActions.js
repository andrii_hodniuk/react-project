import {newWordsPair} from '../../functions/newWordsPair';
import validate from '../../functions/validator';
import fb from '../../services/firebase';
import {
	ADD_NEW_WORD,
	DELETE_WORD,
	GET_USER_DICTIONARY,
	INPUT_WORDS,
	SET_LEARNED_WORD,
	SET_SEARCHED_DICTIONARY,
	CLEAR_SEARCHED_DICTIONARY, CLEAR_INPUT_WORDS,
} from '../types';

export function getUserDictionary() {
	return (dispatch, getState) => {
		const urlRequest = `/wordsList/${getState().user.userUid}`;
		fb.database.ref(urlRequest).on('value', res => {
			dispatch({type: GET_USER_DICTIONARY, payload: res.val() || []});
		});
	};
}

export function setLearnedWord(id) {
	return (dispatch, getState) => {
		const urlRequest = `/wordsList/${getState().user.userUid}`;
		const dictionary = getState().data.currentUserDictionary;
		const newDict = [...dictionary].map(w => {
			if (w.id === id) {
				w.learned = !w.learned;
			}
			return w;
		});
		fb.database
			.ref(urlRequest)
			.set(newDict)
			.then(dispatch({type: SET_LEARNED_WORD, payload: newDict}));
	};
}

export function deleteWord(id) {
	return (dispatch, getState) => {
		const urlRequest = `/wordsList/${getState().user.userUid}`;
		const dictionary = getState().data.currentUserDictionary;
		const newDict = [...dictionary].filter(word => word.id !== id);
		fb.database
			.ref(urlRequest)
			.set(newDict)
			.then(dispatch({type: DELETE_WORD, payload: newDict}));
	};
}

export function inputAddWords(event) {
	return (dispatch, getState) => {
		let words;
		let inputValue = event.target.value.toLowerCase();
		const dictionary = getState().data.currentUserDictionary;
		const {isValid} = validate(inputValue, dictionary);
		if (isValid) {
			words = inputValue.trim().split(/[-!$%^&*()_+|~`{}[:;<>?,.@#\] ]+/).map(w => w.trim());
			dispatch({type: INPUT_WORDS, payload: words});
		} else {
			event.target.value = '';
			dispatch({type: CLEAR_INPUT_WORDS, payload: []});
		}
	};
}

export function addNewWord(event) {
	return (dispatch, getState) => {
		event.preventDefault();
		const dictionary = getState().data.currentUserDictionary;
		const urlRequest = `/wordsList/${getState().user.userUid}`;
		const input = getState().data.inputWords;
		if (input.length > 0) {
			const newWord = newWordsPair(input[0], input[1], dictionary);
			fb.database
				.ref(urlRequest)
				.set([newWord, ...dictionary])
				.then(dispatch({type: ADD_NEW_WORD, payload: {newWord, inputWords: []}}));
		}
	};
}

export function searchWord(event) {
	return (dispatch, getState) => {
		const dictionary = getState().data.currentUserDictionary;
		const value = getState().data.inputWords;
		event.preventDefault();
		if (value.length > 0) {
			const newDict = [...dictionary]
				.filter(word => word.rus.indexOf(value) === 0 || word.eng.indexOf(value) === 0);
			dispatch({
				type: SET_SEARCHED_DICTIONARY,
				payload: {refresh: true, inputWords: [], searchedDictionary: newDict},
			});
		} else {
			dispatch({
				type: CLEAR_SEARCHED_DICTIONARY,
				payload: {refresh: false, searchedDictionary: null},
			});
		}
	};
}
