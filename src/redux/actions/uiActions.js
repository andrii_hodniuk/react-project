import {HIDE_ERROR, SHOW_ERROR} from '../types';


export function showError(text) {
	return dispatch => {
		dispatch({
			type: SHOW_ERROR,
			payload: text,
		});
		setTimeout(() => {
			dispatch(hideError());
		}, 5000);
	};
}

export function hideError() {
	return {
		type: HIDE_ERROR,
	};
}