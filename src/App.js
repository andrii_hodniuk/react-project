import React, {Component} from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import {setUserInfo} from './redux/actions/userActions';
import s from './App.module.css';
import Main from './components/Pages/Main';
import Login from './components/Pages/Authorization/Login';
import {Spin} from 'antd';


class App extends Component {

	componentDidMount() {
		this.props.setUserInfo();
	}

	render() {
		const {user} = this.props;

		if (user === null) {
			return (
				<div className={s.spin}>
					<Spin size="large" />
				</div>
			);
		}

		return (
			<div className={s.wrap}>
				<Switch>
					{user ?
						<>
							<Redirect to="/" />
							<Route component={Main} path="/" exact />
						</> :
						<>
							<Redirect to="/login" />
							<Route path="/login" component={Login} />
						</>
					}
				</Switch>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.currentUser,
});

const mapDispatchToProps = {
	setUserInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

