const validate = (word, data) => {

	const isEmpty = (word) => {
		return word.length === 0;
	};

	const isNumbers = (word) => {
		return word.search(/\d/) !== -1;
	};

	const isSameEng = (word, data) => {
		return data.some(w => w.eng === word);
	};

	const isSameRus = (word, data) => {
		return data.some(w => w.rus === word);
	};

	const isStartWithSymbols = (word) => {
		return word.match(/^[-!$%^&*()_+|~`{}[:;<>?,.@#\] ]+/)
	}

	let errors = [
		isSameEng,
		isSameRus,
		isNumbers,
		isEmpty,
		isStartWithSymbols].reduce((accumulator, checkFunction) => {
		let error = checkFunction(word, data);

		if (error) {
			accumulator.push(error);
		}

		return accumulator;
	}, []);

	return {
		isValid: !errors.length,
	};
};

export default validate;