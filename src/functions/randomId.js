const randomId = () => {
	return Array(15)
		.fill('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
		.map((x) => x[Math.floor(Math.random() * x.length)])
		.join('');
};

export default randomId;