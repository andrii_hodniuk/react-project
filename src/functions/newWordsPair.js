import randomId from './randomId';

export function newWordsPair(word = '', translate = '', dictionary) {
	let engWord;
	let rusWord;
	if (word !== translate) {
		engWord = word.charCodeAt(0) > 96 && word.charCodeAt(0) < 123 ? word : translate;
		rusWord = translate.charCodeAt(0) > 123 ? translate : word;
	} else {
		engWord = word.charCodeAt(0) > 96 && word.charCodeAt(0) < 123 ? word : '***';
		rusWord = translate.charCodeAt(0) > 123 ? translate : '***';
	}
	if (translate === '') {
		if (word.charCodeAt(0) > 96 && word.charCodeAt(0) < 123) {
			engWord = word;
			rusWord = '***';
		} else {
			engWord = '***';
			rusWord = word;
		}
	}
	const rId = randomId();
	const id = () => {
		return dictionary.some(w => w.id === rId) ?
			randomId() :
			rId;
	};
	return {
		eng: `${engWord}`,
		rus: `${rusWord}`,
		id: id(),
		learned: false,
	};
}