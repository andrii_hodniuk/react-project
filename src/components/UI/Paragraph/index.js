import React from 'react';

const P = ({ paragraphClass, children }) => {
    return <p className={paragraphClass}>{children}</p>
}

export default P;