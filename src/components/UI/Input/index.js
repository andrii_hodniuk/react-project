import React from 'react';

const Input = ({ inputType, inputClass, inputValue, inputEventHandler, name }) => {
    return (
        <div>
            <input name={name} type={inputType} className={inputClass} value={inputValue} onChange={inputEventHandler} />
        </div>
    )
}

export default Input;