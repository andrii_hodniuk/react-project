import React from 'react';
import P from '../../Paragraph';

const CardItem = ({ handlerClick, word, translate, pClassFront, pClassBack, cardInnerClass, cardClass }) => {
    return (
        <div className={cardClass.join(' ')} onClick={handlerClick}>
            <div className={cardInnerClass}>
                <P paragraphClass={pClassFront}>
                    {word}
                </P>
                <P paragraphClass={pClassBack}>
                    {translate}
                </P>
            </div>
        </div>
    );
}

export default CardItem;