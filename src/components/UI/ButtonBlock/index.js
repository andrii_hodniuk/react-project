import React from 'react';

const ButtonBlock = ({buttonBlockClass, children}) => {
    return (
        <div className={buttonBlockClass}>
            {children}
        </div>
    )
}

export default ButtonBlock;