import React from 'react';
import s from '../UIComponents.module.css';

const Button = ({ Icon = null, iconStyle = null, handlerClick = null, btnStyle = null, btnClass = null, buttonType = null, children }) => {

    const btn = `${s.mainBlockStyle} ${s.centering}`;
    const classes = [btn];
    if (btnClass) {
        classes.push(btnClass);
    }

    return (
        <button
            type={buttonType}
            className={classes.join(' ')}
            style={btnStyle}
            onClick={handlerClick}
        >
            {Icon ? <Icon style={iconStyle} /> : children}
        </button>
    )

}

export default Button;