import React from 'react';

const H1 = ({ headerClass, children }) => {
    return <h1 className={headerClass}>{children}</h1>
}

export default H1;