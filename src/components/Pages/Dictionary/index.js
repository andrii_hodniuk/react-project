import React from 'react';
import P from '../../UI/Paragraph';
import DictionaryList from './DictionaryList';
import s from './DictionaryBlock.module.css';
import MainPageButton from '../../Navigation/MainPageButton';
import DictionaryInput from './DictionaryInput';

const DictionaryBlock = ({onDictionary, data, showLogOut}) => {
	return (
		<>
			{showLogOut ? <MainPageButton onDictionary={onDictionary} /> : null}

			<ul className={s.dictUl}>
				<DictionaryInput />
				{data.length > 0 ?
					data.map(w => (
						<DictionaryList
							key={w.id} rus={w.rus} eng={w.eng} id={w.id}
							learned={w.learned}
						/>
					))
					: <P paragraphClass={s.noWords}>There are no words in Dictionary!</P>
				}
			</ul>
		</>
	);
};

export default DictionaryBlock;