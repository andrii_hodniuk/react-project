import React from 'react';
import s from './DictionaryInputBtns.module.css';
import DictionaryAdd from './DictionaryAdd';
import DictionarySearch from './DictionarySearch';
import ButtonBlock from '../../../../UI/ButtonBlock';

const DictionaryInputBtns = () => {
    return (
        <ButtonBlock buttonBlockClass={s.btnsWrap}>
            <DictionaryAdd />
            <DictionarySearch />
        </ButtonBlock>
    )
}

export default DictionaryInputBtns;