import React from 'react';
import {connect} from 'react-redux';
import {searchWord} from '../../../../../../redux/actions/dataActions';
import s from './DictionarySearch.module.css';
import {SearchOutlined, SyncOutlined} from '@ant-design/icons';
import Button from '../../../../../UI/Button';

const DictionarySearch = ({searchWord, refresh}) => {
	return (
		<Button btnClass={s.searchBtn} handlerClick={(e) => searchWord(e)}>
			{refresh ? <SyncOutlined style={{fontSize: '40px'}} /> :
				<SearchOutlined style={{fontSize: '40px'}} />}
		</Button>
	);
};

const mapStateToProps = state => ({
	refresh: state.data.refresh,
});

const mapDispatchToProps = {
	searchWord,
};

export default connect(mapStateToProps, mapDispatchToProps)(DictionarySearch);
