import React from 'react';
import {connect} from 'react-redux';
import {addNewWord} from '../../../../../../redux/actions/dataActions';
import s from './DictionaryAdd.module.css';
import {PlusOutlined} from '@ant-design/icons';
import Button from '../../../../../UI/Button';

const DictionaryAdd = ({addNewWord}) => {
	return (
		<Button btnClass={s.addBtn} handlerClick={(e) => addNewWord(e)} buttonType={`submit`}>
			<PlusOutlined style={{fontSize: '30px'}} />
		</Button>
	);
};
const mapDispatchToProps = {
	addNewWord,
};

export default connect(null, mapDispatchToProps)(DictionaryAdd);