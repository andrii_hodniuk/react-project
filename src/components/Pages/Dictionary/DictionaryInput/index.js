import React from 'react';
import {connect} from 'react-redux';
import {inputAddWords} from '../../../../redux/actions/dataActions';
import s from './DictionaryInput.module.css';
import DictionatyInputBtns from './DictionaryInputBtns';
import Input from '../../../UI/Input';
import P from '../../../UI/Paragraph';

const DictionaryInput = ({inputAddWords, value}) => {

	return (
		<>
			<P paragraphClass={s.dictP}>Input new word in dictionary in this format - "word, translation"</P>
			<form className={s.wrap}>
				<Input type={`text`} inputClass={s.dictInput} inputEventHandler={(e) => inputAddWords(e)} inputValue={value} />
				<DictionatyInputBtns />
			</form>
		</>
	);
};

const mapStateToProps = state => ({
	value: state.data.inputWords
})

const mapDispatchToProps = {
	inputAddWords,
};

export default connect(mapStateToProps, mapDispatchToProps)(DictionaryInput);