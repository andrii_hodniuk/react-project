import React from 'react';
import {connect} from 'react-redux';
import {setLearnedWord, deleteWord} from '../../../../redux/actions/dataActions';
import s from './DictionaryList.module.css';
import {DeleteOutlined, CheckOutlined} from '@ant-design/icons';
import Button from '../../../UI/Button';


const DictionaryList = ({eng, rus, id, learned, setLearnedWord, deleteWord}) => {

	const classes = [s.dictList];

	if (learned) {
		classes.push(s.learnedWord);
	}

	return (
		<li className={classes.join(' ')}>
			<Button
				btnClass={s.check}
				handlerClick={() => setLearnedWord(id)}
			>
				<CheckOutlined style={{padding: '20px'}} />
			</Button>
			{eng} - {rus}
			<Button btnClass={s.delete} handlerClick={() => deleteWord(id)}>
				<DeleteOutlined style={{padding: '20px'}} />
			</Button>

		</li>
	);
};

const mapDispatchToProps = {
	setLearnedWord, deleteWord,
};

export default connect(null, mapDispatchToProps)(DictionaryList);
