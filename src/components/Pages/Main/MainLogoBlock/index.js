import React from 'react';
import { ReactComponent as ReactLogoSvg } from '../../../../logo.svg';

const MainLogoBlock = () => {
    return <ReactLogoSvg />
}

export default MainLogoBlock;