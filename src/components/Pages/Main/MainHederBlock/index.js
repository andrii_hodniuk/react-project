import React from 'react';
import s from './MainHeaderBlock.module.css';
import H1 from '../../../UI/Headers/H1';

const MainHeaderBlock = ({ title }) => {
    return <H1 headerClass={s.header}>{title}</H1>
}

export default MainHeaderBlock;