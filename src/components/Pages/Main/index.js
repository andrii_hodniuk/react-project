import React from 'react';
import {connect} from 'react-redux';
import {getUserDictionary} from '../../../redux/actions/dataActions';
import fb from '../../../services/firebase';
import Button from '../../UI/Button';
import P from '../../UI/Paragraph';
import s from './MainBlock.module.css';
import MainLogoBlock from './MainLogoBlock';
import MainHeaderBlock from './MainHederBlock';
import MainDescription from './MainDescriptionBlock';
import Navigation from '../../Navigation/MainNavigation';
import DictionaryBlock from '../Dictionary';
import PracticeBlock from '../Practice';

class MainBlock extends React.Component {

	state = {
		isDictionary: false,
		isPractice: false,
		showLogOut: true,
	};

	componentDidMount() {
		const {getDictionary} = this.props;
		window.addEventListener('scroll', this.onScroll);
		getDictionary();
	}

	onDictionary = () => {
		this.setState({
			isDictionary: !this.state.isDictionary,
		});
	};

	onPractice = () => {
		this.setState({
			isPractice: !this.state.isPractice,
		});
	};

	onScroll = () => {
		if (window.pageYOffset < 50) {
			this.setState({
				showLogOut: true,
			});
		} else {
			this.setState({
				showLogOut: false,
			});
		}
	};

	pageBlock = () => {
		const {userEmail, dictionary, searchedDictionary} = this.props;
		const {isDictionary, isPractice} = this.state;
		const newData = () => searchedDictionary ? searchedDictionary : dictionary;

		if (isDictionary) {
			return <DictionaryBlock
				onDictionary={this.onDictionary}
				data={newData()}
				showLogOut={this.state.showLogOut}
			/>;
		}
		if (isPractice) {
			return <PracticeBlock
				onPractice={this.onPractice}
				data={newData()}
				showLogOut={this.state.showLogOut}
			/>;
		} else {
			return (
				<>
					{this.state.showLogOut ?
						<>
							<div className={s.logOut}>
								<P paragraphClass={s.user}>{userEmail}</P>
								<Button btnClass={s.logoutBtn} handlerClick={fb.logOut}>LogOut</Button>
							</div>
						</> : null
					}
					<MainHeaderBlock title={'Learn words online'} />
					<MainLogoBlock />
					<MainDescription description={'Use cards to memorize and replenish active vocabulary'} />
					<Navigation onDictionary={this.onDictionary} onPractice={this.onPractice} />
				</>
			);
		}
	};

	render() {
		return (
			<div className={s.cover}>
				<div className={s.wrap}>
					{this.pageBlock()}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	userEmail: state.user.currentUser.email,
	userUid: state.user.currentUser.uid,
	dictionary: state.data.currentUserDictionary,
	searchedDictionary: state.data.searchedDictionary,
});

const mapDispatchToProps = {
	getDictionary: () => getUserDictionary(),
};

export default connect(mapStateToProps, mapDispatchToProps)(MainBlock);