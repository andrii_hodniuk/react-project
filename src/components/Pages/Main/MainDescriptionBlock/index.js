import React from 'react';
import s from './MainDescriptionBlock.module.css';
import P from '../../../UI/Paragraph';

const MainDescriptionBlock = ({ description }) => {
    return <P paragraphClass={s.descr}>{description}</P>
}

export default MainDescriptionBlock;