import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {showError} from '../../../../redux/actions/uiActions';
import fb from '../../../../services/firebase';
import ButtonBlock from '../../../UI/ButtonBlock';
import s from './Login.module.css';
import Button from '../../../UI/Button';
import Input from '../../../UI/Input';
import P from '../../../UI/Paragraph';

class Login extends Component {
	state = {
		isLoginPressed: false,
		isRegistrationPressed: false,
	};

	componentDidMount() {
		this.setState({
			isLoginPressed: false,
			isRegistrationPressed: false,
		});
	}

	onSendData = (e) => {
		e.preventDefault();
		const email = e.target.email.value;
		const password = e.target.password.value;
		const {showError} = this.props;

		try {
			if (this.state.isLoginPressed) {
				fb.signWithEmail(email, password)
					.then(<Redirect to="/" />)
					.catch(e => {
						showError(e.message);
					});
			}
		} catch (e) {
		}

		try {
			if (this.state.isRegistrationPressed) {
				fb.createUser(email, password)
					.then(<Redirect to="/" />)
					.catch(e => {
						showError(e.message);
					});
			}
		} catch (e) {
		}

	};

	onLogin = () => {
		this.setState({
			isLoginPressed: true,
		});
	};

	onRegister = () => {
		this.setState({
			isRegistrationPressed: true,
		});
	};

	render() {
		const {error} = this.props;
		return (
			<form className={s.loginForm} onSubmit={this.onSendData}>
				{error ? <P paragraphClass={s.errorMessage}>{error}</P> : null}
				<P paragraphClass={s.loginP}>*Username</P>
				<Input inputClass={s.loginInput} name="email" inputType={'email'} />
				<P paragraphClass={s.loginP}>*Password</P>
				<Input inputClass={s.loginInput} name="password" inputType={'password'} />
				<ButtonBlock buttonBlockClass={s.btnBlock}>
					<Button btnClass={s.loginBtn} handlerClick={this.onLogin}>LogIn</Button>
					<Button btnClass={s.regBtn} handlerClick={this.onRegister}>Registration</Button>
				</ButtonBlock>
			</form>
		);
	}
}

const mapStateToProps = state => ({
	error: state.ui.error,
});

const mapDispatchToProps = {
	showError,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);