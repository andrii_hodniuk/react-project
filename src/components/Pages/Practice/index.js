import React from 'react';
import s from './PracticeBlock.module.css';
import Card from './Card';
import MainPageButton from '../../Navigation/MainPageButton'

const PrecticeBlock = ({ onPractice, data,showLogOut }) => {

    return (
        <>
            {showLogOut ? <MainPageButton onPractice={onPractice} /> : null}
            <div className={s.cardsList}>
                {data.map(w => (
                    <Card key={`${w.eng}-${w.rus}`} rus={w.rus} eng={w.eng} />
                ))}
            </div>
        </>
    )

}

export default PrecticeBlock;