import s from './Card.module.css';
import React from 'react';
import CardItem from '../../../UI/Card/CardItem';

class Card extends React.Component {

    state = {
        done: false
    }

    onCardClick = () => {
        this.setState(() => ({
            done: !this.state.done
        }))
    }

    render() {
        const { done } = this.state;
        const { eng, rus } = this.props;
        const cardClass = [s.card];
        if (done) {
            cardClass.push(s.done)
        }
        return (
            <CardItem
                word={eng}
                translate={rus}
                cardClass={cardClass}
                cardInnerClass={s.cardInner}
                pClassFront={s.cardFront}
                pClassBack={s.cardBack}
                handlerClick={this.onCardClick} />
        );
    }

}

export default Card;