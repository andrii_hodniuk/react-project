import React from 'react';
import Button from '../../UI/Button';
import { HomeOutlined } from '@ant-design/icons';
import s from './MainPageButton.module.css';

const iconStyle = { fontSize: '40px' };

const MainPageButton = ({ onDictionary, onPractice }) => {
    if (onDictionary) {
        return <Button handlerClick={onDictionary} btnClass={s.toMainPage} iconStyle={iconStyle}><HomeOutlined /></Button>
    }
    if (onPractice) {
        return <Button handlerClick={onPractice} btnClass={s.toMainPage} iconStyle={iconStyle}><HomeOutlined /></Button>
    }
}

export default MainPageButton;