import React from 'react';
import s from './NavigateBlock.module.css';
import Button from '../../UI/Button/index'
import ButtonBlock from '../../UI/ButtonBlock';



const NavigateBlock = ({ onDictionary, onPractice }) => {
    return (
        <ButtonBlock buttonBlockClass={s.navWrap}>
            <Button btnClass={s.navBtn} handlerClick={onDictionary}>
                Dictionary
            </Button>
            <Button btnClass={s.navBtn} handlerClick={onPractice}>
                Practice
            </Button>
        </ButtonBlock>
    );
}

export default NavigateBlock; 